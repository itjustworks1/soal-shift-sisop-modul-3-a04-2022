#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#define MAX 1024
#define P 5000

struct sockaddr_in address;
char buffer[1024] = {0};

int sockets = 0, val;
struct sockaddr_in serv_addr;
char *hello = "From client";

void leave_server();
void client_process();

int main()
{
  bool isLogIn = false;

  if ((sockets = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("\nCreation error\n");
    return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(P);

  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
  {
    printf("\nInvalid address\n");
    return -1;
  }

  if (connect(sockets, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("\nConnection Failed\n");
    return -1;
  }

  while (true)
    client_process();

  return 0;
}

void client_process()
{
  val = read(sockets, buffer, MAX);
  if (strcmp(buffer, "RMSG") == 0)
  {
    char input[MAX];
    scanf("%s", input);
    send(sockets, input, MAX, 0);
  }
  else if (strcmp(buffer, "SMSG") == 0)
  {
    val = read(sockets, buffer, MAX);
    printf("%s", buffer);
  }
  else
  {
    printf("unkown: %s", buffer);
  }
  strcpy(buffer, "");
}

void leave_server()
{
  send(sockets, "BYE", 3, 0);
}
