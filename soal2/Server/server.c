#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#define P 5000
#define MAX 1024

int addrlen = sizeof(address);
int option = 1;
int server_id, new_sock, value;
struct sockaddr_in address;
char *hello = "SIGN IN";
char buffer[1024] = {0};

enum state
{
  INIT,
  LOGIN,
  REGISTER
};
enum state States;

void send_msg(char str[]);
void read_client();
bool validate_pass(char str[]);
bool user_found(char str[]);
bool do_regist();
bool do_login();
void wait_new_client();
void server_process();

int main()
{
  if ((server_id = socket(AF_INET, SOCK_STREAM, 0)) == 0)
  {
    perror("socket failed");
    exit(EXIT_FAILURE);
  }

  if (setsockopt(server_id, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option)))
  {
    perror("setsockopt");
    exit(EXIT_FAILURE);
  }

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(P);

  if (bind(server_id, (struct sockaddr *)&address, sizeof(address)) < 0)
  {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  if (listen(server_id, 3) < 0)
  {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  wait_new_client();

  while (true)
    server_process();
  return 0;
}

void send_msg(char str[])
{
  send(new_sock, "SMSG", MAX, 0);
  send(new_sock, str, MAX, 0);
}

void read_client()
{
  send(new_sock, "RMSG", MAX, 0);
  strcpy(buffer, "");
  value = read(new_sock, buffer, MAX);
  printf("%s\n", buffer);
}

bool validate_pass(char str[])
{
  if (strlen(str) < 6)
  {
    send_msg("pass to short\n");
    return false;
  }
  bool hasNum, hasUpper, hasLower;
  hasNum = hasUpper = hasLower = false;
  for (int i = 0; i < strlen(str); i++)
  {
    if (str[i] >= 'a' && str[i] <= 'z')
      hasLower = true;
    if (str[i] >= 'A' && str[i] <= 'Z')
      hasUpper = true;
    if (str[i] >= '0' && str[i] <= '9')
      hasNum = true;
  }
  if (!hasNum)
    send_msg("must have a number\n");
  if (!hasUpper)
    send_msg("must have an uppercase letter\n");
  if (!hasLower)
    send_msg("must have an lowercase letter\n");
  return (hasLower && hasUpper && hasNum);
}

bool user_found(char str[])
{
  char filepath[] = "../users.txt";
  FILE *fp;
  bool userFound = false;
  size_t len = MAX;
  char *inp = NULL;
  if (!(fp = fopen(filepath, "r")))
    send_msg("user.txt not found!\n");
  while (!userFound)
  {
    if (getline(&inp, &len, fp) == -1)
      break;
    char *uname = strtok(inp, ":");
    if (strcmp(str, uname) == 0)
      userFound = true;
  }
  fclose(fp);
  return userFound;
}

bool do_regist()
{
  char uname[MAX], upass[MAX];
  send_msg("Username: ");
  read_client();
  strcpy(uname, buffer);
  if (user_found(uname))
  {
    send_msg("username already exists!\n");
    return false;
  }
  send_msg("Password: ");
  read_client();
  strcpy(upass, buffer);
  if (!validate_pass(upass))
    return false;
  char filepath[] = "../users.txt";
  FILE *fp;
  fp = fopen(filepath, "a");
  fprintf(fp, "%s:%s\n", uname, upass);
  send_msg("user added\n");
  fclose(fp);
  return true;
}

bool do_login()
{
  char uname[MAX], upass[MAX];
  send_msg("enter username: ");
  read_client();
  strcpy(uname, buffer);
  send_msg("enter password: ");
  read_client();
  strcpy(upass, buffer);

  FILE *fp;
  bool userFound = false;
  size_t len = MAX;
  char *inp = NULL;
  char filepath[] = "../users.txt";

  if (!(fp = fopen(filepath, "r")))
    send_msg("user.txt not found!\n");
  while (!userFound)
  {
    if (getline(&inp, &len, fp) == -1)
      break;
    char *n = strtok(inp, ":");
    char *p = strtok(NULL, "\n");
    if (strcmp(n, uname) == 0 && strcmp(p, upass) == 0)
      userFound = true;
  }
  send_msg((userFound ? "log in success\n" : "wrong username or password\n"));
  return userFound;
}

void wait_new_client()
{
  if ((new_sock = accept(server_id, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
  {
    perror("accept");
    exit(EXIT_FAILURE);
  }
  States = INIT;
}

void server_process()
{
  switch (States)
  {
  case INIT:
    send_msg("(register/login)\n");
    read_client();
    if (strcmp(buffer, "register") == 0)
      States = REGISTER;
    else if (strcmp(buffer, "login") == 0)
      States = LOGIN;
    else
      send_msg("invalid input\n");
    break;
  case LOGIN:
    do_login();
    break;
  case REGISTER:
    if (do_regist())
      States = INIT;
    break;
  default:
    break;
  }
  strcpy(buffer, "");
}
