# soal-shift-sisop-modul-2-a04-2022 

|     NRP    |     Nama    |
| :--------- |:------------    |
| 5025201098 | Ibra Abdi Ibadihi
| 5025201184 | Cahyadi Surya Nugraha |
| 5025201007 | Sejati Bakti Raga

# Soal 1
## Catatan
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak
- Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2

## A. Download dan Unzip File
Proses download dan unzip file yang disediakan dilakukan dengan fungsi download_file. Untuk pendownloadan digunakan fungsi execv yang menjalankan fungsi wget dengan argumen url file yang telah diberikan pada soal. pthread_equal digunakan untuk membandingkan thread ID dan menjalankannya pada waktu yang sama sesuai permintaan soal. 

```
#define MUSIC_URL "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"
#define QUOTE_URL "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download"
#define PATH "~/Documents/praktikum-sisop/praktikum-3/soal1"
#define MUSIC "music.zip"
#define QUOTE "quote.zip"
#define HASIL "hasil.zip"

void *download_file(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, tid[0]))
  {
    int status;
    if (fork() == 0)
    {
      char *argv[] = {"mkdir", "-p", "music", NULL};
      execv("/usr/bin/mkdir", argv);
    }
    else
    {
      while ((wait(&status)) > 0)
        ;
      int status2;

      if (fork() == 0)
      {
        char *argv[] = {"wget", "-q", "--no-check-certificate", MUSIC_URL, "-O", MUSIC, NULL};
        execv("/usr/bin/wget", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"unzip", "-qq", MUSIC, "-d", "music", NULL};
        execv("/usr/bin/unzip", argv);
      }

      while ((wait(&status2)) > 0)
        ;
    }
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    int status;
    if (fork() == 0)
    {
      char *argv[] = {"mkdir", "-p", "quote", NULL};
      execv("/usr/bin/mkdir", argv);
    }
    else
    {
      while ((wait(&status)) > 0)
        ;
      int status2;
      if (fork() == 0)
      {
        char *argv[] = {"wget", "-q", "--no-check-certificate", QUOTE_URL, "-O", QUOTE, NULL};
        execv("/usr/bin/wget", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"unzip", "-qq", QUOTE, "-d", "quote", NULL};
        execv("/usr/bin/unzip", argv);
      }

      while ((wait(&status2)) > 0)
        ;

      if (fork() == 0)
      {
        char *argv[] = {"mkdir", "-p", "hasil", NULL};
        execv("/usr/bin/mkdir", argv);
      }

      while ((wait(&status2)) > 0)
        ;
    }
  }
}
```

## B. Decode File
Di dalam soal kita diminta untuk melakukan decode file .txt dengan base 64. Pertama, kita buat tabel untuk decoding.

```
char base64[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
char *decoding_table;
```
Decoding file, dilakukan menggunakan fungsi decoding.

```
void decoding(char *txt)
{
  char buff[4], result[100];
  int i = 0, index = 0, count = 0, j;

  while (txt[i] != '\0')
  {
    for (j = 0; j < 64 && base64[j] != txt[i]; j++)
      ;
    buff[count++] = j;
    if (count == 4)
    {
      result[index++] = (buff[0] << 2) + (buff[1] >> 4);
      if (buff[2] != 64)
        result[index++] = (buff[1] << 4) + (buff[2] >> 2);
      if (buff[3] != 64)
        result[index++] = (buff[2] << 6) + buff[3];
      count = 0;
    }
    i++;
  }

  result[index] = '\0';
  strcpy(res, result);
}
```
Proses decoding dilakukan dengan menjalankan fungsi decode yang akan memanggil fungsi decoding. Fungsi decode akan menjalankan dua thread yang masing-masing akan membuka file quote dan music dan memanggil fungsi decoding untuk mendecode file tersebut. Hasil decoding akan diappend pada file baru pada folder hasil sesuai jenisnya (quote atau music) kemudian ditutup. 
```
#define MUSIC_FOLDER "./music/"
#define QUOTE_FOLDER "./quote/"
#define HASIL_FOLDER "./hasil/"
#define HASIL_MUSIC_TXT "./hasil/music.txt"
#define HASIL_QUOTE_TXT "./hasil/quote.txt"

void *decode(void *arg)
{
  pthread_t id = pthread_self();

  if (pthread_equal(id, tid[0]))
  {
    DIR *directory;
    struct dirent *dir1;
    directory = opendir(MUSIC_FOLDER);

    while ((dir1 = readdir(directory)) != NULL)
    {
      if (strstr(dir1->d_name, ".txt") && strcmp(dir1->d_name, ".") && strcmp(dir1->d_name, ".."))
      {
        char temp_name_dir[FILENAME_MAX], loc[1024], buffer[1024];
        strcpy(temp_name_dir, MUSIC_FOLDER);
        strcat(temp_name_dir, dir1->d_name);
        FILE *file_music = fopen(temp_name_dir, "r");
        if (file_music != NULL)
          fgets(buffer, 1024, file_music);
        else
          printf("Task Failed :[Open File Failed]\n");
        fclose(file_music);
        decoding(buffer);
        FILE *file_hasil = fopen(HASIL_MUSIC_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
        music_count++;
      }
    }
    closedir(directory);
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    DIR *directory;
    struct dirent *dir2;
    directory = opendir(QUOTE_FOLDER);
    while ((dir2 = readdir(directory)) != NULL)
    {
      if (strstr(dir2->d_name, ".txt") && strcmp(dir2->d_name, ".") && strcmp(dir2->d_name, ".."))
      {
        char temp_name_dir[FILENAME_MAX];
        char loc[1024], buffer[1024];
        strcpy(temp_name_dir, QUOTE_FOLDER);
        strcat(temp_name_dir, dir2->d_name);
        FILE *file_quote = fopen(temp_name_dir, "r");
        if (file_quote != NULL)
          fgets(buffer, 1024, file_quote);
        else
          printf("Task Failed :[Open File Failed]\n");
        fclose(file_quote);
        decoding(buffer);
        FILE *file_hasil = fopen(HASIL_QUOTE_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
        quote_count++;
      }
    }
    closedir(directory);
  }
}

```

## C. Pemindahan File ke Folder Hasil
Pemindahan file ke folder hasil terjadi pada akhir fungsi decode yang ada di poin B.

```
#define HASIL_MUSIC_TXT "./hasil/music.txt"
#define HASIL_QUOTE_TXT "./hasil/quote.txt"

.
.
.
        FILE *file_hasil = fopen(HASIL_MUSIC_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
.
.
.
        FILE *file_hasil = fopen(HASIL_QUOTE_TXT, "a");
        fprintf(file_hasil, "%s\n", res);
        fclose(file_hasil);
.
.
.
```

## D. Pengezipan dan password
Pengezipan dan password dilakukan dengan fungsi zip_file dengan password yang telah didefinisikan di awal.

```
#define PASSWORD "mihinomenestcahyadi"
void zip_file()
{
  int status;
  child_process = fork();
  if (child_process == 0)
  {
    char *cmd[] = {"zip", "-P", PASSWORD, "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&status)) > 0)
    ;
}
```

## E. Unzip dan Rezip
Unzip dan rezip dilakukan menggunakan fungsi add_file_to_zip yang menggunakan thread untuk menjalankan fungsi pengunzipan dengan execv dan pembuatan file no.txt pada waktu yang sama. pthread_equal sekali lagi digunakan untuk membandingkan threadID.

```
#define HASIL_NO_TXT "./hasil/no.txt"
void *add_file_to_zip(void *arg)
{
  pthread_t id = pthread_self();
  if (pthread_equal(id, tid[0]))
  {
    if (fork() == 0)
    {
      char *argv[] = {"unzip", "-P", PASSWORD, "-qq", HASIL, NULL};
      execv("/usr/bin/unzip", argv);
    }
  }
  else if (pthread_equal(id, tid[1]))
  {
    pthread_join(tid[0], NULL);

    FILE *file_no = fopen(HASIL_NO_TXT, "a");
    fprintf(file_no, "No");
    fclose(file_no);
  }
}
```

Setelah itu dilakukan pengezipan file dengan fungsi zip_file lagi seperti pada poin D.

```
void zip_file()
{
  int status;
  child_process = fork();
  if (child_process == 0)
  {
    char *cmd[] = {"zip", "-P", PASSWORD, "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&status)) > 0)
    ;
}
```

## Output

#### 1. Hasil download dan unzip file
![satu](img/1.png)

#### 2. Stat file music dan quote yang dibuat pada waktu yang sama
![dua](img/2.png)

#### 3. Isi file music
![lima](img/5.png)

#### 4. Isi file quote
![enam](img/6.png)

#### 5. Isi file decoding hasil
![tiga](img/3.png)

#### 6. File zip dengan password
![empat](img/4.png)

## Kendala
- Membuat fungsi yang tepat baik dari parameter dan logicnya sehingga tidak membuat code terlalu panjang dan lebih mudah dibaca
- File hasil tidak terzip dengan benar jika menggunakan WSL
